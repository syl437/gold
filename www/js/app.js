// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','ngSanitize'])

.run(function($rootScope,$ionicPlatform,$http,$localStorage) {
	
	 //$templateCache.put('edit_user_page.html', '');
	
	 $rootScope.DefaultLanguage = "HEB";
	 $rootScope.host = "http://www.tapper.co.il/gold/php/";
	 $rootScope.InformationArray  = '';
	 $rootScope.loadedOnce  = 0;
	 $rootScope.LanguageJson = '';
	 
	  $http.get('js/json/languages.json').success(function(data)
	{
		$rootScope.LanguageJson = data;
	});


	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  
    .state('app.NewUser', {
      url: '/NewUser',
      views: {
        'menuContent': {
          templateUrl: 'templates/new_user.html',
          controller: 'NewUser'
        }
      }
    })
	
  .state('app.CodeLogin', {
    url: '/CodeLogin/:id/:codeid',
    views: {
      'menuContent': {
        templateUrl: 'templates/code_login.html',
        controller: 'CodeLogin'
      }
    }
  })	
  .state('app.EditProfile', {
    url: '/EditProfile/:id/:codeid',
    views: {
      'menuContent': {
        templateUrl: 'templates/edit_profile.html',
        controller: 'EditProfile'
      }
    }
  })		
  .state('app.EditUser', {
    url: '/EditUser/:id/:codeid/:contactindex',
    views: {
      'menuContent': {
        templateUrl: 'templates/edit_user_page.html',
        controller: 'EditUser'
      }
    }
  })
  .state('app.Main', {
    url: '/Main',
    views: {
      'menuContent': {
        templateUrl: 'templates/main.html',
        controller: 'MainController'
      }
    }
  })
  .state('app.NewUserPage', {
    url: '/NewUserPage/:id/:codeid',
    views: {
      'menuContent': {
        templateUrl: 'templates/new_user_page.html',
        controller: 'NewUserPageC'
      }
    }
  })
  .state('app.OrderCode', {
    url: '/OrderCode',
    views: {
      'menuContent': {
        templateUrl: 'templates/order_code_page.html',
        controller: 'OrderCode'
      }
    }
  })  
  .state('app.LogIn', {
    url: '/LogIn',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LogIn'
      }
    }
  }) 
  .state('app.NewCode', {
    url: '/NewCode/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/new_code_page.html',
        controller: 'NewCode'
      }
    }
  })
  .state('app.EditCode', {
    url: '/EditCode/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/edit_code_page.html',
        controller: 'EditCode'
      }
    }
  })
  .state('app.CodeSelect', {
    url: '/CodeSelect/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/code_select_page.html',
        controller: 'CodeSelect'
      }
    }
  })  
  .state('app.FoundCode', {
    url: '/FoundCode',
    views: {
      'menuContent': {
        templateUrl: 'templates/found_code.html',
        controller: 'FoundCode'
      }
    }
  }) 
  .state('app.FoundCodeLogin', {
    url: '/FoundCodeLogin/:userid/:codeid',
    views: {
      'menuContent': {
        templateUrl: 'templates/found_code_login.html',
        controller: 'FoundCodeLogin'
      }
    }
  }) 
  .state('app.Thanks', {
    url: '/Thanks',
    views: {
      'menuContent': {
        templateUrl: 'templates/thanks_page.html',
        controller: 'ThanksPage'
      }
    }
  })  
  .state('app.Register', {
    url: '/Register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
        controller: 'Register'
      }
    }
  }) 
  .state('app.FoundPhoneLogin', {
    url: '/FoundPhoneLogin/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/foundphone.html',
        controller: 'FoundPhoneLogin'
      }
    }
  })  
  .state('app.About', {
    url: '/About',
    views: {
      'menuContent': {
        templateUrl: 'templates/about_page.html',
        controller: 'About'
      }
    }
  })
  .state('app.BuyCodes', {
    url: '/BuyCodes',
    views: {
      'menuContent': {
        templateUrl: 'templates/buy_page.html',
        controller: 'BuyCodesController'
      }
    }
  })

  .state('app.Contact', {
    url: '/Contact',
    views: {
      'menuContent': {
        templateUrl: 'templates/contact.html',
        controller: 'ContactController'
      }
    }
  }); 

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/Main');
});
