angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage,$ionicPopup,$rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
  
  $scope.logout = function()
  {
			$localStorage.loggedinuserid = '';
			$localStorage.usercredits = '';
			$localStorage.fullname = '';	
			$localStorage.phone = '';
			$localStorage.mail = '';			
			window.location.href = "#/app/Main";
  }
  $scope.reloadMenu = function()
  {
	  $scope.defLang = $rootScope.DefaultLanguage;
  }
  
  $scope.defLang = $rootScope.DefaultLanguage;
  
  $scope.myProducts = function()
  {
	if ($localStorage.loggedinuserid) {
	  window.location.href = "#/app/CodeSelect/"+$localStorage.loggedinuserid;
	}
	else 
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$ionicPopup.alert({
			title: "יש להתחבר תחילה",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			$ionicPopup.alert({
			title: "you must sign in first",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		
	}
	
	  
	  
  }
	$scope.Foundphone = function () 
	{
		
	if ($localStorage.loggedinuserid) {
	  window.location.href = "#/app/FoundPhoneLogin/"+$localStorage.loggedinuserid;
	}
	
	else 
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$ionicPopup.alert({
			title: "יש להתחבר תחילה",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			$ionicPopup.alert({
			title: "you must sign in first",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		
	}	

	}
})



.controller('MainController', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicModal,$ionicPopup,$ionicLoading,$cordovaBarcodeScanner) {
  	
	$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
	$scope.ChangeLanguage = function(lang)
	{
		if (lang == 1)
		{
			$rootScope.DefaultLanguage = "ENG";
			$scope.MainImages();
			$scope.getInfo();
			$scope.checkLanguage();
			$scope.reloadMenu();
			
		}
		else 
		{
			$rootScope.DefaultLanguage = "HEB";
			$scope.MainImages();
			$scope.getInfo();
			$scope.checkLanguage();
			$scope.reloadMenu();
			
		}
	}	
	$scope.MainImages = function()
	{
		$scope.LogoImage =  $rootScope.LanguageJson.Main.Logo[$rootScope.DefaultLanguage];
		$scope.NewUserImage =  $rootScope.LanguageJson.Main.newUser[$rootScope.DefaultLanguage];
		$scope.ExisingUserImage =  $rootScope.LanguageJson.Main.exisitingUser[$rootScope.DefaultLanguage];
		$scope.FoundProductImage =  $rootScope.LanguageJson.Main.foundProduct[$rootScope.DefaultLanguage];
		$scope.FoundPhoneImage =  $rootScope.LanguageJson.Main.foundPhone[$rootScope.DefaultLanguage];			
	}
	
	$scope.MainImages();
	

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	$scope.checkLanguage();
	
	
	$scope.navigateLocal = function () 
	{
	if ($localStorage.loggedinuserid) {
	window.location.href = "#/app/CodeSelect/"+$localStorage.loggedinuserid;
	}
	else {
		window.location.href = "#/app/LogIn";
	}
	}

	$scope.Foundphone = function () 
	{
	
	if ($localStorage.loggedinuserid) {
	window.location.href = "#/app/FoundPhoneLogin/"+$localStorage.loggedinuserid;
	}
	else 
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$ionicPopup.alert({
			title: "משתמש אינו מחובר",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		else
		{
			$ionicPopup.alert({
			title: "user not logged in",
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		
	}
	}
	
	$scope.navigatePage = function (Path,Num) 
	{
		window.location.href = Path+String(Num);	
	}		
	
	$scope.InformationBtn = function () 
	{
		window.location.href = "#/app/About";
	}	

	
	//get information
	if ($rootScope.loadedOnce == 0)
	{
		$http.get($rootScope.host+'/getInformation.php').success(function(data)
		{
			$rootScope.InformationArray = data;
			//$scope.InformationArray = data;
			//console.log($rootScope.InformationArray);
			//alert($rootScope.InformationArray);
			$rootScope.loadedOnce  = 1;
			$scope.getInfo();
		});			
	}
	
	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[0].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[0].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[0].english_text;
		}
		
		//alert ($scope.InfoHidden)
	}
	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}
	
	if ($localStorage.loggedinuserid)
	{
		//window.location.href = "#/app/CodeSelect/"+$localStorage.loggedinuserid;
	}
	


	
	$scope.ScanQR = function()
	{
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	
		
		cordova.plugins.barcodeScanner.scan(
		  function (result) {
			if(!result.cancelled)
			{
				$ionicLoading.hide();
								  
				  $scope.Split = result.text.split("=");
				  
				  if ($scope.Split[1])
				  {
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

					search_params = 
					{
						'code' : $scope.Split[1]
					};

					$http.post($rootScope.host+'/lookup_unique.php', search_params)
					.success(function(data, status, headers, config)
					{
					
					var status = data.response.status;
					//console.log(response);
					
					if (status == 0) {
						
						userid = data.response.userid;
						codeid = data.response.code;
						window.location.href = "#/app/FoundCodeLogin/"+userid+"/"+codeid+"";

					}
					
					else 
					{
						$ionicPopup.alert({
						title: $rootScope.LanguageJson.Main.codeNotFound[$rootScope.DefaultLanguage],
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });	

					}

					})
					.error(function(data, status, headers, config)
					{
						//alert("f : " +data.responseText)
						//console.log('error : ' + data);
					});						  
				  }
				  else
				  {
					  $ionicPopup.alert({
						title: $rootScope.LanguageJson.Main.codeNotFound[$rootScope.DefaultLanguage],
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });
				  }				
				
			}
			else
			{
			  $ionicLoading.hide();
			}
		  },
		  function (error) {
			  $ionicLoading.hide();
			  
			$ionicPopup.alert({
			 title: "שגיאה בסריקה: " + error,
			buttons: [{
				text: 'אשר',
				type: 'button-positive',
			  }]
			});			  
		  }
		);
	}
	
	
})

.controller('About', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		$scope.welcomeText = $rootScope.LanguageJson.AboutPage.AboutText[$rootScope.DefaultLanguage];
	
		$scope.navigatePage = function (Path,Num) 
		{
			window.location.href = Path+String(Num);	
		}
		
		$scope.navigate = function (Path) 
		{
			window.location.href = Path;	
		}
		
		$scope.backBtn = function (Path,Num) 
		{
			window.history.back();
		}
		
			$scope.HomeButton = function (Path,Num) 
		{

			window.location.href = "#/app/Main";

		}
		
		$scope.calling = function (Phone) 
		{
			window.location.href = "tel:+1231231234"
		}
		
		$scope.waze = function (Waze) 
		{
			window.location.href = "waze://?ll=' + Waze + '&navigate=yes";	
		}
		
		$scope.sendMail = function (Mail) 
		{
			window.location.href = "mailto:' + Mail + '";	
		}
		
})

.controller('NewUser', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicModal,$ionicPopup) {
	
		$scope.ButtonImage =  $rootScope.LanguageJson.NewUser.HeaderImage[$rootScope.DefaultLanguage];
		$scope.NamePlaceHolder = $rootScope.LanguageJson.NewUser.fullName[$rootScope.DefaultLanguage];
		$scope.PhonePlaceHolder = $rootScope.LanguageJson.NewUser.phone[$rootScope.DefaultLanguage];
		$scope.EmailPlaceHolder = $rootScope.LanguageJson.NewUser.email[$rootScope.DefaultLanguage];
		$scope.okButton = $rootScope.LanguageJson.NewUser.okButton[$rootScope.DefaultLanguage];
		$scope.TermsConditionHolder = $rootScope.LanguageJson.NewUser.TermsAgree[$rootScope.DefaultLanguage];
		$scope.TermsText = $rootScope.LanguageJson.NewUser.TermsText[$rootScope.DefaultLanguage];

		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		
		$scope.fields = 
		{
			"name" : "",
			"phone" : "",
			"mail" : "",
			"agree" : false
		}
		$scope.AgreeCheck = function()
		{
			if ($scope.fields.agree)
			{
				$scope.fields.agree = false;
			}
			else
			{
				$scope.fields.agree = true;
			}
		}

		$scope.checkTable = function () 
		{
			
			if ($scope.fields.name =="")
			{
				$ionicPopup.alert({
				title: $scope.NamePlaceHolder,
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
			}
			else if ($scope.fields.phone =="")
			{
				$ionicPopup.alert({
				title: $scope.PhonePlaceHolder,
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
			}
			else if ($scope.fields.mail =="")
			{
				$ionicPopup.alert({
				title: $scope.EmailPlaceHolder,
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
			}
			else if (!$scope.fields.agree)
			{
				$ionicPopup.alert({
				title: $scope.TermsConditionHolder,
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });					
			}			


			
			else 
			{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				/*
				$scope.name = angular.element('#nameInput').val();
				$scope.phone = angular.element('#phoneInput').val();
				$scope.mail = angular.element('#mailInput').val();
				*/

				data = 
				{
					'name' : $scope.fields.name ,
					'phone' : $scope.fields.phone ,
					'mail' : $scope.fields.mail
					//'code' : $scope.code 
				};

				$http.post($rootScope.host+'/enter_new_user.php', data)
				.success(function(data, status, headers, config)
				{
				var userid = data.response.userid;
				//var codeid = data.response.codeid;
				var checklogin = data.response.login;
				//alert (checklogin);
				if (checklogin =="0") 
				{
					
				//$scope.loggedinuserid = $localStorage;
				$localStorage.loggedinuserid = userid;
				$localStorage.usercredits = 0;
				$localStorage.fullname = $scope.fields.name;
				$localStorage.phone = $scope.fields.phone;	
				$localStorage.mail = $scope.fields.mail;					
				
				window.location.href = "#/app/CodeSelect/"+userid;
				}
				else 
				{
					if ($rootScope.DefaultLanguage == "HEB")
					{				
						$ionicPopup.alert({
						title: "טלפון או מייל כבר קיימים במערכת יש להתחבר כמשתמש קיים",
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });
		   
					}
					else
					{			
						$ionicPopup.alert({
						title: "Phone or mail already in use , please login instead.",
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });
		   
					}
					
				}

				})
				.error(function(data, status, headers, config)
				{

				});				
			}
			

			
		}		

	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[1].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[1].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[1].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();
	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}
	
})

.controller('LogIn', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicModal,$ionicPopup) {
	
		$scope.MailPlaceholder =  $rootScope.LanguageJson.NewUser.email[$rootScope.DefaultLanguage];
		$scope.PhonePlaceholder =  $rootScope.LanguageJson.NewUser.phone[$rootScope.DefaultLanguage];
		$scope.OkButton =  $rootScope.LanguageJson.NewUser.okButton[$rootScope.DefaultLanguage];
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		

		$scope.fields = 
		{
			"mail" : "",
			"phone" : ""
		}
		$scope.checklogin = function () 
		{
			
			if ($scope.fields.mail == "")
			{
				$ionicPopup.alert({
				title: $scope.MailPlaceholder,
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });				
			}
			else if ($scope.fields.phone == "")
			{
				$ionicPopup.alert({
				title: $scope.PhonePlaceholder,
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });				
			}
			else
			{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				data = 
				{
					'mail' : $scope.fields.mail,
					'phone' : $scope.fields.phone
				};

				$http.post($rootScope.host+'/user_login.php', data)
				.success(function(data, status, headers, config)
				{
				
				var response = data.response;
				
				if (response && response.login =="0") 
				{
				//var usercredits = response.credits;	
				//$scope.loggedinuserid = $localStorage;
				$localStorage.fullname = response.name;
				$localStorage.phone = response.phone;
				$localStorage.mail = response.mail;
				$localStorage.loggedinuserid = response.userid;
				$localStorage.usercredits = response.credits;
				window.location.href = "#/app/CodeSelect/"+response.userid;
				}
				else 
				{
					if ($rootScope.DefaultLanguage == "HEB")
					{
						$ionicPopup.alert({
						title: 'כתובת מייל או טלפון שגוים יש לנסות שוב',
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });
				   
					}
					else 
					{
						$ionicPopup.alert({
						title: 'incorrect email or phone please try again',
						buttons: [{
							text: 'OK',
							type: 'button-positive',
						  }]
					   });
					}	
				}
				})
				.error(function(data, status, headers, config)
				{
					//alert("f : " +data.responseText)
					//console.log('error : ' + data);
				});				
			}	
		}	

	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[2].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[2].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[2].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();

	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}		
})

.controller('CodeSelect', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicModal,$ionicPopup) {
	
	
	$scope.$on('$ionicView.enter', function(e) {
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		$scope.currentCreditText =  $rootScope.LanguageJson.CodeSelect.currentCredit[$rootScope.DefaultLanguage];
		$scope.AddNewCodeImage =  $rootScope.LanguageJson.CodeSelect.newcode[$rootScope.DefaultLanguage];
		$scope.LogOutImage =  $rootScope.LanguageJson.CodeSelect.logout[$rootScope.DefaultLanguage];
		$scope.CodeNumberText =  $rootScope.LanguageJson.CodeSelect.codeNumber[$rootScope.DefaultLanguage];
		$scope.EditCodeImage =  $rootScope.LanguageJson.CodeSelect.editcode[$rootScope.DefaultLanguage];
		$scope.DeleteCodeImage =  $rootScope.LanguageJson.CodeSelect.deletecode[$rootScope.DefaultLanguage];
		$scope.welcomeText =  $rootScope.LanguageJson.CodeSelect.welcomeText[$rootScope.DefaultLanguage];

		$scope.LogOutBtn =  $rootScope.LanguageJson.CodeSelect.LogOutBtn[$rootScope.DefaultLanguage];
		$scope.NewCodeBtn =  $rootScope.LanguageJson.CodeSelect.NewCodeBtn[$rootScope.DefaultLanguage];		

		$scope.id = $localStorage.loggedinuserid;
		$scope.usercredits = $localStorage.usercredits;
		$scope.userfullname = $localStorage.fullname;
		
			
		$scope.getusercodeslist = function()
		{
			//userid = $scope.id;
			$http.get($rootScope.host+'/usercodes.php?user='+$localStorage.loggedinuserid)
			.success(function(data, status, headers, config)
			{
			$scope.usercodes = data.codes;
			$scope.qrimage = data.codes.code;
			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
			});		
		}
		
		$scope.getusercodeslist();
	
		$scope.editCode = function (codeid) {
			window.location.href = "#/app/EditCode/"+codeid+"";
		}
		$scope.deleteCode = function (codeid,codename) 
		{


			if ($rootScope.DefaultLanguage == "HEB")
			{
				$scope.confirmtext  = 'האם לאשר מחיקת קוד מספר '+codename+'?';
			}
			else
			{
				$scope.confirmtext  = 'confirm delete of code number '+codename+'?';
			}
			
			
		
		   var confirmPopup = $ionicPopup.confirm({
			 title: $scope.confirmtext,
			 //template: 'Are you sure you want to eat this ice cream?'
		   });

		   confirmPopup.then(function(res) {
			 if(res) 
			 {
				$http.get($rootScope.host+'/delete_code.php?id='+codeid)
				.success(function(data, status, headers, config)
				{
			
				})
				.error(function(data, status, headers, config)
				{
					//alert("f : " +data.responseText)
					//console.log('error : ' + data);
				});
		
				$scope.getusercodeslist();
				//window.location.href = "#/app/CodeSelect/"+$scope.id+"/";
			 } 
		   });

   

			

		}
		
		$scope.LoginBtn = function (userid,codeid) 
		{
			window.location.href = "#/app/CodeLogin/"+userid+"/"+codeid+"";
		}
		
		$scope.logoutBtn = function (userid,codeid) 
		{
		
			$localStorage.loggedinuserid = '';
			$localStorage.usercredits = '';
			$localStorage.fullname = '';
			$localStorage.phone = '';
			$localStorage.mail = '';
				
			window.location.href = "#/app/LogIn/";
		}
		
		$scope.newcodepage = function () 
		{
			if ($localStorage.usercredits <= 0)
			{
				if ($rootScope.DefaultLanguage == "HEB")
				{
					
					$ionicPopup.alert({
					title: "אין בחשבונך מספיק קרדיט להוספת קוד חדש , הינך מעובר לדף רכישת קודים",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });				   
				}
				else
				{					
					$ionicPopup.alert({
					title: "not enough credits to add new code,redirecting to purchase page",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });				   
				}
				window.location.href = "#/app/BuyCodes";
			}
			else 
			{
				window.location.href = "#/app/NewCode/"+$scope.id+"";
			}
			
		}
		
	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[5].hidden;
		
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[5].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[5].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();
	
	$scope.ShowInfo = function()
	{
		
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}	
	});
})

.controller('EditCode', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		$scope.ProuductPlaceHolder =  $rootScope.LanguageJson.NewCode.productName[$rootScope.DefaultLanguage];
		$scope.CodePlaceHolder =  $rootScope.LanguageJson.NewCode.Code[$rootScope.DefaultLanguage];
		$scope.OkButtonImage =  $rootScope.LanguageJson.NewCode.okButton[$rootScope.DefaultLanguage];
	
		$scope.id = $stateParams.id;	
		$scope.fields = 
		{
			"code" : "",
			"prdname" : ""
		}
		
		if ($scope.id)
		{
			$http.get($rootScope.host+'/get_code_data.php?id='+$stateParams.id)
			.success(function(data, status, headers, config)
			{
			$scope.fields.prdname = data.response.prdname;
			$scope.fields.code = data.response.code;	
			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
			});
		
		}
		
	$scope.saveCode = function () 
	{
		 if ($scope.fields.prdname =="")
		{
			$ionicPopup.alert({
			title: $scope.ProuductPlaceHolder,	
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		/*
		else if ($scope.fields.code =="")
		{
			$ionicPopup.alert({
			title: $scope.CodePlaceHolder,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}	
		*/
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			data = 
			{
				'codeid' : $stateParams.id,
				'code' : $scope.fields.code,
				'prdname' : $scope.fields.prdname
				};

			$http.post($rootScope.host+'/edit_code.php', data)
			.success(function(data, status, headers, config)
			{
			var responsedata = data.response.status;
			if (responsedata =="0") {
				window.history.back();
			}
			else {
				if ($rootScope.DefaultLanguage =="HEB")
				{
					$ionicPopup.alert({
					title: "הקוד שהזנת כבר קיים במערכת יש להזין קוד אחר",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });
				   
				}
				else 
				{
					$ionicPopup.alert({
					title: "the code you input already exists , type a different code",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });
				   
				}
				
			}
			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
			});			
		}

	}
})

.controller('FoundPhoneLogin', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicModal,$ionicPopup) {
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		$scope.Text1 =  $rootScope.LanguageJson.FoundPhonePage.Text1[$rootScope.DefaultLanguage];
		$scope.Text2 =  $rootScope.LanguageJson.FoundPhonePage.Text2[$rootScope.DefaultLanguage];
		$scope.Text3 =  $rootScope.LanguageJson.FoundPhonePage.Text3[$rootScope.DefaultLanguage];
		$scope.Text4 =  $rootScope.LanguageJson.FoundPhonePage.Text4[$rootScope.DefaultLanguage];
		$scope.Text5 =  $rootScope.LanguageJson.FoundPhonePage.Text5[$rootScope.DefaultLanguage];
		$scope.orplaceholder =  $rootScope.LanguageJson.FoundPhonePage.or[$rootScope.DefaultLanguage];
		$scope.phoneLabel =  $rootScope.LanguageJson.FoundPhonePage.phone[$rootScope.DefaultLanguage];
		$scope.mailLabel =  $rootScope.LanguageJson.FoundPhonePage.mail[$rootScope.DefaultLanguage];
		$scope.okButtonImage =  $rootScope.LanguageJson.FoundPhonePage.okButton[$rootScope.DefaultLanguage];

		$scope.requireText =  $rootScope.LanguageJson.FoundPhonePage.required[$rootScope.DefaultLanguage];
		$scope.usefulText =  $rootScope.LanguageJson.FoundPhonePage.useful[$rootScope.DefaultLanguage];

		
		$scope.fields = 
		{
			"phone": "",
			"mail" : ""
		}
		
	$scope.lookupusercode = function()
	{
        $http.get($rootScope.host+'/get_usercode_profile.php?id='+$stateParams.id)
        .success(function(data, status, headers, config)
        {
		$scope.username = data.response.name;
		$scope.productname = data.response.prdname;
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
	}
	
	$scope.lookupusercode();
	
	$scope.alertcontacts = function () 
	{
		
		if ($scope.fields.phone =="" && $scope.fields.mail =="")
		{
			$ionicPopup.alert({
			title: $scope.phoneLabel+ ' ' +$scope.orplaceholder+' '+$scope.mailLabel,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		/*
		else if ($scope.fields.mail =="")
		{
			$ionicPopup.alert({
			title: $scope.mailLabel,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		*/
		else
		{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				
		//phone = angular.element('#phoneInput').val();
		//mail = angular.element('#mailInput').val();

		
			data = 
			{	
				//'code' : $routeParams.codeid,
				'userid' : $stateParams.id,
				'phone' : $scope.fields.phone,
				'mail' : $scope.fields.mail,
				'language' : $rootScope.DefaultLanguage


			};

			$http.post($rootScope.host+'/message_contacts.php?type=phone', data)
			.success(function(data, status, headers, config)
			{
			
			//var status = data.response.status;
			//console.log(response);
			
			

			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
			});
			
			window.location.href = "#/app/Thanks";			
		}
		
		

	}
	
	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[4].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.textDir = "rtl";
			$scope.InfoText =  $rootScope.InformationArray[4].hebrew_text;
		}
		else
		{
			$scope.textDir = "ltr";
			$scope.InfoText =  $rootScope.InformationArray[4].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();
	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}	
	
})

.controller('ThanksPage', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		$scope.Text1 =  $rootScope.LanguageJson.ThanksPage.Text1[$rootScope.DefaultLanguage];
		$scope.Text2 =  $rootScope.LanguageJson.ThanksPage.Text2[$rootScope.DefaultLanguage];
		
})

.controller('FoundCode', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicModal,$ionicPopup) {
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		$scope.inputcodePlaceholder =  $rootScope.LanguageJson.FoundProduct.Productcode[$rootScope.DefaultLanguage];
		$scope.okbuttonimage =  $rootScope.LanguageJson.FoundProduct.okButton[$rootScope.DefaultLanguage];
		
		$scope.fields = 
		{
			"code" : ""
		}
		


		
		$scope.checkcode = function () 
		{
		
		if ($scope.fields.code == "")
		{
			$ionicPopup.alert({
			title: $scope.inputcodePlaceholder,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

					
			//checkcode = angular.element('#codeInput').val();
			

			data = 
			{
				'code' : $scope.fields.code
			};

			$http.post($rootScope.host+'/lookup_code.php', data)
			.success(function(data, status, headers, config)
			{
			
			var status = data.response.status;
			//console.log(response);
			
			if (status == 0) {
				
				userid = data.response.userid;
				codeid = data.response.code;
				window.location.href = "#/app/FoundCodeLogin/"+userid+"/"+codeid+"";

			}
			
			else 
			{
				$ionicPopup.alert({
				title: $rootScope.LanguageJson.FoundProduct.codeNotFound[$rootScope.DefaultLanguage],
				buttons: [{
					text: 'OK',
					type: 'button-positive',
				  }]
			   });	

			}

			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
			});			
		}
		

		
		}	
	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[3].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[3].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[3].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();
	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}	
	
})

.controller('FoundCodeLogin', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {
	$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
	$scope.Text1 =  $rootScope.LanguageJson.FoundCodeLogin.Text1[$rootScope.DefaultLanguage];
	$scope.Text2 =  $rootScope.LanguageJson.FoundCodeLogin.Text2[$rootScope.DefaultLanguage];
	$scope.Text3 =  $rootScope.LanguageJson.FoundCodeLogin.Text3[$rootScope.DefaultLanguage];
	$scope.Text4 =  $rootScope.LanguageJson.FoundCodeLogin.Text4[$rootScope.DefaultLanguage];
	$scope.orplaceholder=  $rootScope.LanguageJson.FoundCodeLogin.or[$rootScope.DefaultLanguage];
	$scope.phoneplaceholder=  $rootScope.LanguageJson.FoundCodeLogin.phone[$rootScope.DefaultLanguage];
	$scope.mailplaceholder =  $rootScope.LanguageJson.FoundCodeLogin.mail[$rootScope.DefaultLanguage];
	$scope.okbuttonImage =  $rootScope.LanguageJson.FoundCodeLogin.okButton[$rootScope.DefaultLanguage];
	$scope.requiredText =  $rootScope.LanguageJson.FoundCodeLogin.required[$rootScope.DefaultLanguage];
	$scope.usefulText =  $rootScope.LanguageJson.FoundCodeLogin.useful[$rootScope.DefaultLanguage];

	//$scope.id = $stateParams.id;
	//$scope.codeid = $stateParams.codeid;
	
	$scope.fields = 
	{
		"phone" : "",
		"mail" : ""
	}
	
	$scope.getusercodeprofile = function()
	{

        $http.get($rootScope.host+'/get_usercode_profile.php?id='+$stateParams.userid+'&codeid='+$stateParams.codeid)
        .success(function(data, status, headers, config)
        {
		$scope.username = data.response.name;
		$scope.productname = data.response.prdname;
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });	
	}
	
	$scope.getusercodeprofile();
	
	$scope.alertcontacts = function () 
	{
		
	if ($scope.fields.phone == "" && $scope.fields.mail == "")
	{
		$ionicPopup.alert({
		title: $scope.phoneplaceholder+ ' ' +$scope.orplaceholder+' '+$scope.mailplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	/*
	else if ($scope.fields.mail == "")
	{
		$ionicPopup.alert({
		title: $scope.mailplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	*/
	else
	{
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
	//phone = angular.element('#phoneInput').val();
	//mail = angular.element('#mailInput').val();

		//alert ($stateParams.codeid);
		//alert ($stateParams.userid);
	
        data = 
		{	
			'code' : $stateParams.codeid,
			'userid' : $stateParams.userid,
		    'phone' : $scope.fields.phone,
			'mail' : $scope.fields.mail,
			'language' : $rootScope.DefaultLanguage


        };

        $http.post($rootScope.host+'/message_contacts.php', data)
        .success(function(data, status, headers, config)
        {
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
		
		window.location.href = "#/app/Thanks";		
	}
		
		
		

	}
	
	if ($rootScope.DefaultLanguage == "HEB")
	{
		$scope.textDir = "rtl";
	}
	else
	{
		$scope.textDir = "ltr";
	}	
		
})

.controller('NewCode', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup,$ionicModal) {
	$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
	$scope.ProuductPlaceHolder =  $rootScope.LanguageJson.NewCode.productName[$rootScope.DefaultLanguage];
	$scope.CodePlaceHolder =  $rootScope.LanguageJson.NewCode.Code[$rootScope.DefaultLanguage];
	$scope.OkButtonImage =  $rootScope.LanguageJson.NewCode.okButton[$rootScope.DefaultLanguage];
	
	
	$scope.fields = 
	{
		"code" : "",
		"prdname" : ""
	}
	
	$scope.addnewcode = function () 
	{

	 if ($scope.fields.prdname =="")
	{
		$ionicPopup.alert({
		title: $scope.ProuductPlaceHolder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	/*
	else if ($scope.fields.code =="")
	{
		$ionicPopup.alert({
		title: $scope.CodePlaceHolder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	*/
	else
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		

        data = 
		{
			'user' : $stateParams.id,
            'code' : $scope.fields.code,
			'prdname' : $scope.fields.prdname
        };

        $http.post($rootScope.host+'/enter_new_code.php', data)
        .success(function(data, status, headers, config)
        {
	
		var checkcode = data.response.code;
		if (checkcode == "0") 
		{ 
	
		//update credits
		
		$localStorage.usercredits = parseInt($localStorage.usercredits-1);
		window.location.href = "#/app/CodeSelect/"+$stateParams.id+"";
		
		
		}
		else 
		{
			if ($rootScope.DefaultLanguage =="HEB")
			{
					$ionicPopup.alert({
					title: "קוד כבר בשימוש יש להזין קוד אחר",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });
				   
			}
			else 
			{
					$ionicPopup.alert({
					title: "code already in use , use a different code name",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });				
			}
			
			
		}
		
		//console.log(data)
            //console.log(status + ' - ' + data.responseText);
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });	


		
	}

		
	}
	
	
	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[8].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[8].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[8].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();
	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}		
	

	
		
})

.controller('BuyCodesController', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicModal,$cordovaInAppBrowser,$ionicPopup) {
$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
$scope.addCredits = '';

$scope.fields = 
{
	"package" : ""
}


/*
 var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };

    $cordovaInAppBrowser.open('http://ngcordova.com', '_blank', options)
      .then(function(event) {
        // success
      })
      .catch(function(event) {
        // error
      });
*/
	/*
	var pusher = new Pusher('4e4e5deeb95324f203a1', {
	  encrypted: false
	});
	var channel = pusher.subscribe('GoldApp');
	channel.bind($localStorage.userid, function(data) 
	{
		if (data.status == 1)
		{
			alert ("עסקה בוצעה בהצלחה");
		}
		else 
		{
			alert ("עסקה נכשלה יש לנסות שוב");
		}	
	})
	*/

		$scope.CurrentCredit =  $rootScope.LanguageJson.BuyPage.CurrentCredit[$rootScope.DefaultLanguage];
		$scope.Package1Title =  $rootScope.LanguageJson.BuyPage.Package1Title[$rootScope.DefaultLanguage];
		$scope.Package1Desc =  $rootScope.LanguageJson.BuyPage.Package1Desc[$rootScope.DefaultLanguage];
		$scope.Package1Price =  $rootScope.LanguageJson.BuyPage.Package1Price[$rootScope.DefaultLanguage];
		
		$scope.Package2Title =  $rootScope.LanguageJson.BuyPage.Package2Title[$rootScope.DefaultLanguage];
		$scope.Package2Desc =  $rootScope.LanguageJson.BuyPage.Package2Desc[$rootScope.DefaultLanguage];
		$scope.Package2Price =  $rootScope.LanguageJson.BuyPage.Package2Price[$rootScope.DefaultLanguage];
	
		$scope.Package3Title =  $rootScope.LanguageJson.BuyPage.Package3Title[$rootScope.DefaultLanguage];
		$scope.Package3Desc =  $rootScope.LanguageJson.BuyPage.Package3Desc[$rootScope.DefaultLanguage];
		$scope.Package3Price =  $rootScope.LanguageJson.BuyPage.Package3Price[$rootScope.DefaultLanguage];		
	
		$scope.TransOk1 =  $rootScope.LanguageJson.BuyPage.TransOkOne[$rootScope.DefaultLanguage];		
		$scope.TransOk2 =  $rootScope.LanguageJson.BuyPage.TransOkTwo[$rootScope.DefaultLanguage];		
		$scope.BadTran =  $rootScope.LanguageJson.BuyPage.BadTrans[$rootScope.DefaultLanguage];		
	
	//alert ($localStorage.loggedinuserid);
	$scope.usercredits = $localStorage.usercredits;
	$scope.fields.package = 1;
	
	
	$scope.choosePackage = function(id)
	{
		$scope.fields.package = id;
	}
	
	
	if ($rootScope.DefaultLanguage == "HEB")
	{
		$scope.BuyLang = 'heb';
	}
	else
	{
		$scope.BuyLang = 'eng';
	}
	
	
	
	$scope.selectPackage = function() 
	{
		$scope.selectedpackage = $scope.fields.package;
		//var ref = '';
		if ($scope.selectedpackage == 1)
		{
			iabRef = window.open("https://direct.tranzila.com/avisherf/iframe.php?sum=15&currency=1&user="+$localStorage.loggedinuserid+"&lang="+$scope.BuyLang, '_blank', 'location=yes');
			$scope.addCredits = 15;
		}
		else if ($scope.selectedpackage == 2)
		{
			iabRef = window.open("https://direct.tranzila.com/avisherf/iframe.php?sum=30&currency=1&user="+$localStorage.loggedinuserid+"&lang="+$scope.BuyLang, '_blank', 'location=yes');
			$scope.addCredits = 30;
		}
		else if ($scope.selectedpackage == 3)
		{
			iabRef = window.open("https://direct.tranzila.com/avisherf/iframe.php?sum=45&currency=1&user="+$localStorage.loggedinuserid+"&lang="+$scope.BuyLang, '_blank', 'location=yes');
			$scope.addCredits = 45;
		}
		//alert ($scope.selectedpackage)
		

         iabRef.addEventListener('exit', iabClose);
		 iabRef.addEventListener('loadstart', iabLoadStart);
         iabRef.addEventListener('loadstop', iabLoadStop);
		 
		 
	}
	
	 function iabClose(event) 
	{

         iabRef.removeEventListener('exit', iabClose);
    }
	
    function iabLoadStart(event) {
        //alert(event.type + ' - ' + event.url);
    }

    function iabLoadStop(event) 
	{
        //alert(event.type + ' - ' + event.url);
		
	var url1 = event.url;
    var check1 = "tapper";

	var url2 = event.url;
    var check2 = "?status=1";
	
	//good
	if (url1.indexOf(check1) > -1 && url2.indexOf(check2) > -1)
	{

			$ionicPopup.alert({
			title: $scope.TransOk1 + " " + $scope.addCredits + " "+ $scope.TransOk2,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });
				   
				   
		$localStorage.usercredits = parseInt($localStorage.usercredits+$scope.addCredits);
		iabRef.close();
		window.location.href = "#/app/CodeSelect/"+$localStorage.loggedinuserid;

	}
	//bad
	else if (url1.indexOf(check1) > -1 )
	{
			$ionicPopup.alert({
			title: $scope.BadTran,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });
		   
		iabRef.close();
	}

	
	 //iabRef.removeEventListener('loadstop', iabLoadStop);

    }



	
	//$scope.ref.addEventListener(exit, $scope.exitbrowser);
	

	
	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[7].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[7].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[7].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();
	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}		
	
	
	
})

.controller('CodeLogin', function($scope,$http,$stateParams,$localStorage,$rootScope,$cordovaPrinter,$ionicModal,$ionicPopup) {
  $scope.$on('$ionicView.enter', function(e) {
  		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		$scope.PrintQRImage =  $rootScope.LanguageJson.CodeLogin.printQR[$rootScope.DefaultLanguage];
		$scope.PrintMailQRImage =  $rootScope.LanguageJson.CodeLogin.printMailImage[$rootScope.DefaultLanguage];
		$scope.AddContactImage =  $rootScope.LanguageJson.CodeLogin.AddContact[$rootScope.DefaultLanguage];
		$scope.AddQRImage =  $rootScope.LanguageJson.CodeLogin.mailQR[$rootScope.DefaultLanguage];
		$scope.mailSent =  $rootScope.LanguageJson.CodeLogin.mailSent[$rootScope.DefaultLanguage];
		$scope.ThanksQR =  $rootScope.LanguageJson.CodeLogin.Thanks[$rootScope.DefaultLanguage];
		$scope.ChooseSize =  $rootScope.LanguageJson.CodeLogin.ChooseSize[$rootScope.DefaultLanguage];
		$scope.TextDirection =  $rootScope.LanguageJson.CodeLogin.direction[$rootScope.DefaultLanguage];

		$scope.id = $stateParams.id;
		$scope.codeid = $stateParams.codeid;
		$scope.unique = '';
		$scope.PrintId = 0;

	$scope.getUserProfile = function()
	{
	
       $http.get($rootScope.host+'/get_profile.php?id='+$localStorage.loggedinuserid)
        .success(function(data, status, headers, config)
        {
			
		//console.log(contacts);
	
		$scope.profilename = data.userprofile.name;
		$scope.profilephone = data.userprofile.phone;
		$scope.profilemail = data.userprofile.email;


		//console.log(data)
            //console.log(status + ' - ' + data.responseText);
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });	
	}	

	$scope.getUserProfile();

	$scope.getusercontacts = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
	
        $http.get($rootScope.host+'/usercontacts.php?codeid='+$localStorage.loggedinuserid)
        .success(function(data, status, headers, config)
        {
			
		$scope.contacts = data.contacts;
		

		//console.log(data)
            //console.log(status + ' - ' + data.responseText);
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
		
	}
	
	$scope.getusercontacts();


	$scope.getqrcode = function()
	{
		
		$http.get($rootScope.host+'/qr_code.php?id='+$localStorage.loggedinuserid+'&codeid='+$scope.codeid)
		.success(function(data, status, headers, config)
		{
		//console.log(contacts);
		
		$scope.qrUrl = data.response.url;
		$scope.fullcode = data.response.code;
		$scope.unique = data.response.unique;
		$scope.prdname = data.response.prdname;
		//alert ($scope.prdname)
		
		//angular.element('#qrcodediv').html('<img src="'+response+'" width="200" height="200">');
		
		})
		.error(function(data, status, headers, config)
		{
			//alert("f : " +data.responseText)
			//console.log('error : ' + data);
		});
	}	
	
	$scope.getqrcode();
	
	$scope.printQR = function (divid) 
	{
		//window.open("http://tapper.co.il/gold/printqr.php?unique="+$scope.unique+"&lang="+$rootScope.DefaultLanguage, '_blank', 'location=yes');
		
		
		/*
		var printerAvail = $cordovaPrinter.isAvailable();
		printContents = document.getElementById(divid).innerHTML;
		$cordovaPrinter.print(printContents);
		*/
		/*
		printContents = document.getElementById(divid).innerHTML;
		popupWin = window.open('', '_blank', 'width=300,height=300');
		popupWin.document.open()
		popupWin.document.write(printContents);
		popupWin.window.print();
		popupWin.window.close();			
		*/
	}
	
	$scope.radioselect = 
	{
		"size" : "70"
	}

	$scope.mailBtn = function(id)
	{
	   $scope.PrintId = id;
	  $ionicModal.fromTemplateUrl('templates/mail_modal.html', {
		scope: $scope
	  }).then(function(mailModal) {
		$scope.mailModal = mailModal;
		$scope.mailModal.show();
		});			
	}
	
	$scope.closeMailModal = function()
	{
		$scope.mailModal.hide();
	}		
	
	$scope.sendQr= function()
	{
		if ($scope.PrintId == 0)
		{
			if($cordovaPrinter.isAvailable()) {
				$cordovaPrinter.print("http://tapper.co.il/gold/printqr.php?unique="+$scope.unique+"&lang="+$rootScope.DefaultLanguage+"&size="+$scope.radioselect.size);
			} else {
				alert("Printing is not available on device");
			}			
		}
		else
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			send_data = 
			{
				//'id' : $localStorage.loggedinuserid ,
				'unique' : $scope.unique,
				'mail' : $localStorage.mail,
				'name' : $localStorage.fullname,
				'size' : $scope.radioselect.size,
				'language' : $rootScope.DefaultLanguage,
				'send' : 1
			};

			$http.post($rootScope.host+'/mail_qr.php', send_data)
			.success(function(data, status, headers, config)
			{	
			})
			.error(function(data, status, headers, config)
			{
			});

		
			$ionicPopup.alert({
			title: $scope.mailSent+ ' ' +$localStorage.mail+ ' ' +  $scope.ThanksQR,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });				
		}

	   $scope.mailModal.hide();
	}
	$scope.newcontactpage = function () 
	{
		window.location.href = "#/app/NewUserPage/"+$scope.id+"/"+$scope.codeid+"";
	}	
	
	$scope.editprofile = function () 
	{
	window.location.href = "#/app/EditProfile/"+$scope.id+"/"+$scope.codeid+"";
	}


	$scope.getInfo = function()
	{
		$scope.InfoHidden = $rootScope.InformationArray[6].hidden;
		
		if ($rootScope.DefaultLanguage == "HEB")
		{
			$scope.InfoText =  $rootScope.InformationArray[6].hebrew_text;
		}
		else
		{
			$scope.InfoText =  $rootScope.InformationArray[6].english_text;
		}
		
	}
	
	$scope.getInfo();

	$scope.checkLanguage = function()
	{
		if ($rootScope.DefaultLanguage =="HEB")
		{
			$scope.ExplainModal = 'הסבר';
			$scope.closeModal = 'סגירה';
		}
		else
		{
			$scope.ExplainModal = 'Tutorial';
			$scope.closeModal = 'Close';
		}
	}
	
	$scope.checkLanguage();
	
	$scope.ShowInfo = function()
	{
	  $ionicModal.fromTemplateUrl('templates/info_modal.html', {
		scope: $scope
	  }).then(function(infoModal) {
		$scope.infoModal = infoModal;
		$scope.infoModal.show();
		});			
	}
	
	$scope.closeInfoModal = function()
	{
		$scope.infoModal.hide();
	}		
	
	
	
});		
})

.controller('EditProfile', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {
		$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
		
		$scope.nameplaceholder =  $rootScope.LanguageJson.EditProfile.name[$rootScope.DefaultLanguage];
		$scope.phoneplaceholder =  $rootScope.LanguageJson.EditProfile.phone[$rootScope.DefaultLanguage];
		$scope.emailplaceholder=  $rootScope.LanguageJson.EditProfile.email[$rootScope.DefaultLanguage];
		$scope.okbuttonimage =  $rootScope.LanguageJson.EditProfile.okButton[$rootScope.DefaultLanguage];
		//alert ($scope.PrintQRImage)
		
		$scope.id = $stateParams.codeid;	
		
	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : ""
	}
		$scope.getUserProfile = function()
	{
	
       $http.get($rootScope.host+'/get_profile.php?id='+$localStorage.loggedinuserid)
        .success(function(data, status, headers, config)
        {
			
		//console.log(contacts);
	
		$scope.fields.name = data.userprofile.name;
		$scope.fields.phone = data.userprofile.phone;
		$scope.fields.mail = data.userprofile.email;


		//console.log(data)
            //console.log(status + ' - ' + data.responseText);
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });	
	}	

	$scope.getUserProfile();
	

	$scope.edituser = function()
	{
		
	if ($scope.fields.name == "")
	{
		$ionicPopup.alert({
		title: $scope.nameplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}	
	else if ($scope.fields.phone == "")
	{
		$ionicPopup.alert({
		title: $scope.phoneplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}	
	else if ($scope.fields.mail == "")
	{
		$ionicPopup.alert({
		title: $scope.emailplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	else
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
		data = 
		{
			'id' : $localStorage.loggedinuserid ,
			'name' : $scope.fields.name,
			'phone' : $scope.fields.phone,
			'mail' : $scope.fields.mail
		};

		$http.post($rootScope.host+'/update_user.php?id='+$localStorage.loggedinuserid, data)
		.success(function(data, status, headers, config)
		{	
		//var response = data.response;
		//console.log(response);	
		})
		.error(function(data, status, headers, config)
		{
			//alert("f : " +data.responseText)
			//console.log('error : ' + data);
		});
		

		window.location.href = "#/app/CodeLogin//"+$scope.id;		
	}
		
		


		
	}
	
})

.controller('NewUserPageC', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {
	$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
	$scope.nameplaceholder =  $rootScope.LanguageJson.NewContact.name[$rootScope.DefaultLanguage];
	$scope.phoneplaceholder =  $rootScope.LanguageJson.NewContact.phone[$rootScope.DefaultLanguage];
	$scope.emailplaceholder=  $rootScope.LanguageJson.NewContact.email[$rootScope.DefaultLanguage];
	$scope.orplaceholder=  $rootScope.LanguageJson.NewContact.or[$rootScope.DefaultLanguage];
	$scope.okbuttonimage =  $rootScope.LanguageJson.NewContact.okButton[$rootScope.DefaultLanguage];
		
	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : ""
	}
	
	$scope.addnewcontact = function () 
	{
	
	if ($scope.fields.name =="")
	{
		$ionicPopup.alert({
		title: $scope.nameplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	else if ($scope.fields.phone =="" &&  $scope.fields.mail =="")
	{
		$ionicPopup.alert({
		title: $scope.phoneplaceholder+ ' ' +$scope.orplaceholder+' '+$scope.emailplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	/*
	else if ($scope.fields.mail =="")
	{
		$ionicPopup.alert({
		title: $scope.emailplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	*/
	else
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


		data = 
		{
			'userid' : $localStorage.loggedinuserid,
			'codeid' : $stateParams.codeid,
			'name' : $scope.fields.name,
			'phone' : $scope.fields.phone,
			'mail' : $scope.fields.mail,
			'language' : $rootScope.DefaultLanguage
		};
		console.log(data)
		//return false;

		$http.post($rootScope.host+'/add_new_contact.php', data)
		.success(function(data, status, headers, config)
		{
		var responsedata = data.response.status;

		if (responsedata =="0") { 
		
			$ionicPopup.alert({
			title: $rootScope.LanguageJson.NewContact.maxContactsError[$rootScope.DefaultLanguage],
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });
		   
		}
		

		})
		.error(function(data, status, headers, config)
		{
			//alert("f : " +data.responseText)
			//console.log('error : ' + data);
		});

		window.location.href = "#/app/CodeLogin//"+$stateParams.codeid;		
	}
		

	}
		
})

.controller('EditUser', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {
	$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
	$scope.nameplaceholder =  $rootScope.LanguageJson.EditContact.name[$rootScope.DefaultLanguage];
	$scope.phoneplaceholder =  $rootScope.LanguageJson.EditContact.phone[$rootScope.DefaultLanguage];
	$scope.emailplaceholder=  $rootScope.LanguageJson.EditContact.email[$rootScope.DefaultLanguage];
	$scope.orplaceholder=  $rootScope.LanguageJson.EditContact.or[$rootScope.DefaultLanguage];
	$scope.okbuttonimage =  $rootScope.LanguageJson.EditContact.okButton[$rootScope.DefaultLanguage];
	$scope.deletecontactImage =  $rootScope.LanguageJson.EditContact.deleteContactBtn[$rootScope.DefaultLanguage];

	$scope.codeid = $stateParams.codeid;
	$scope.contactid = $stateParams.contactindex;
	
	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : ""
	}
	
	$scope.showcontact = function()
	{
		
		
        $http.get($rootScope.host+'/get_single_contact.php?userid='+$scope.contactid)
        .success(function(data, status, headers, config)
        {	
		//var contacts = data.contacts;
			$scope.fields.name = data.contacts.name;
			$scope.fields.phone = data.contacts.phone;
			$scope.fields.mail = data.contacts.email;
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });		
	}
	
	$scope.showcontact();
	
	
	$scope.editcontact = function()
	{
		
	if ($scope.fields.name == "")
	{
		$ionicPopup.alert({
		title: $scope.nameplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}	
	else if ($scope.fields.phone == "" && $scope.fields.mail == "")
	{
		$ionicPopup.alert({
		title: $scope.phoneplaceholder+ ' ' +$scope.orplaceholder+' '+$scope.emailplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	/*
	else if ($scope.fields.mail == "")
	{
		$ionicPopup.alert({
		title: $scope.emailplaceholder,
		buttons: [{
			text: 'OK',
			type: 'button-positive',
		  }]
	   });			
	}
	*/
	else
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		
		//loggedinuser = $routeParams.id;
		//codeid = $routeParams.codeid;
		//contactindex = $routeParams.contactindex;
		
			data = 
			{
				'id' : $scope.contactid,
				'name' : $scope.fields.name,
				'phone' : $scope.fields.phone,
				'mail' : $scope.fields.mail
			};

			$http.post($rootScope.host+'/update_contact.php?id='+$scope.contactid, data)
			.success(function(data, status, headers, config)
			{			
			//var response = data.response;
			//console.log(response);
			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
			});
			
			window.location.href = "#/app/CodeLogin//"+$scope.codeid;
			//window.location.href = "#/CodeLogin/"+loggedinuser+"/"+codeid+"";		
		}
		
	}
	
	
	$scope.deletecontact = function()
	{
		//loggedinuser = $routeParams.id;
		//codeid = $routeParams.codeid;
		//contactindex = $routeParams.contactindex;
	
        $http.get($rootScope.host+'/delete_contact.php?id='+$scope.contactid)
        .success(function(data, status, headers, config)
        {
		//var response = data.response;
		//console.log(response);
        })
        .error(function(data, status, headers, config)
        {
			//alert("f : " +data.responseText)
            //console.log('error : ' + data);
        });
		
		window.location.href = "#/app/CodeLogin//"+$scope.codeid;

	}
	
	
	
})

.controller('ContactController', function($scope,$http,$stateParams,$localStorage,$rootScope,$ionicPopup) {

	$scope.NamePlaceHolder = $rootScope.LanguageJson.Contact.fullName[$rootScope.DefaultLanguage];
	$scope.PhonePlaceHolder = $rootScope.LanguageJson.Contact.phone[$rootScope.DefaultLanguage];
	$scope.EmailPlaceHolder = $rootScope.LanguageJson.Contact.email[$rootScope.DefaultLanguage];
	$scope.TextPlaceHolder = $rootScope.LanguageJson.Contact.text[$rootScope.DefaultLanguage];
	$scope.okButton = $rootScope.LanguageJson.Contact.okButton[$rootScope.DefaultLanguage];
	$scope.navTitle='<img class="title-image" src="img/main/header_logo.png" style="width:160px; margin-top:12px;"/>';
	

		
	$scope.fields = 
	{
		"name" : $localStorage.fullname,
		"phone" : $localStorage.phone,
		"mail" : $localStorage.mail,
		"msg" : ""

	}	
	
	
	$scope.sendContact = function()
	{
		if ($scope.fields.name == "")
		{
			$ionicPopup.alert({
			title: $scope.NamePlaceHolder,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.fields.phone == "")
		{
			$ionicPopup.alert({
			title: $scope.PhonePlaceHolder,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.fields.mail == "")
		{
			$ionicPopup.alert({
			title: $scope.EmailPlaceHolder,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		else if ($scope.fields.msg == "")
		{
			$ionicPopup.alert({
			title: $scope.TextPlaceHolder,
			buttons: [{
				text: 'OK',
				type: 'button-positive',
			  }]
		   });			
		}
		else
		{
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

				send_params = 
				{
					'name' : $scope.fields.name,
					'phone' : $scope.fields.phone,
					'mail' : $scope.fields.mail,
					'msg' : $scope.fields.msg,
					'send' : 1
				};

				
				$http.post($rootScope.host+'/send_contact.php', send_params)
				.success(function(data, status, headers, config)
				{
				})
				.error(function(data, status, headers, config)
				{
				});	

				if ($rootScope.DefaultLanguage == "HEB")
				{				
					$ionicPopup.alert({
					title: "תודה פניתך התקבלה בהצלחה, נחזור אליך בהקדם",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });
	   
				}
				else
				{			
					$ionicPopup.alert({
					title: "Thanks for your feedback , we'll get back to you shortly",
					buttons: [{
						text: 'OK',
						type: 'button-positive',
					  }]
				   });
	   
				}
				
				

				$scope.fields.name = '';
				$scope.fields.phone = '';
				$scope.fields.mail = '';
				$scope.fields.msg = '';
				
				window.location.href = "#/app/Main";
					
		}	
	}
	
	
		
});